//
//  FilmesCollectionViewController.swift
//  Dfilmes
//
//  Created by Henrique Mota on 06/09/17.
//  Copyright © 2017 hkmobi. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ListaFilmesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var listaFilmes = [Filme]()
    
    var pagina = 1
    var pullToRefresh = false
    
    var cache = NSCache<AnyObject, AnyObject>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 2)
        
        getFilmesPopulares(pagina: pagina)
        
        self.collectionView?.addPullToRefresh({ [weak self] in
            self?.pagina = 1
            self?.pullToRefresh = true
            self?.listaFilmes.removeAll()
            self?.collectionView?.reloadData()
            self?.getFilmesPopulares(pagina: (self?.pagina)!)
        })
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        pagina = 1
        listaFilmes.removeAll()
        self.collectionView.reloadData()
        getFilmesPopulares(pagina: pagina)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        searchBar.resignFirstResponder()
        pagina = 1
        listaFilmes.removeAll()
        self.collectionView.reloadData()
        getBuscarFilmes(termo: searchBar.text!)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return listaFilmes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (view.frame.width/2 - 8), height: 250)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! FilmeCollectionViewCell
        
        let filme = listaFilmes[indexPath.row]
        
        cell.nomeFilme.text = filme.title
        cell.anoFilme.text = "\(filme.year!)"
        
        cell.imagemFilme.image = nil
        if let image = cache.object(forKey: filme.ids!.tmdb! as AnyObject) {
            cell.imagemFilme.image = image as? UIImage
        }else {
            cell.imagemFilme.image = nil
            Alamofire.request("https://image.tmdb.org/t/p/w300\(filme.capa!)").responseImage { response in
                if let image = response.result.value {
                    cell.imagemFilme.image = image
                    self.cache.setObject(image, forKey: filme.ids!.tmdb! as AnyObject)
                }
            }
        }
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if (indexPath.row == (listaFilmes.count - 1)){
            if(pagina != 1){
                getFilmesPopulares(pagina: pagina)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "openFilme" {
            let selectRow = self.collectionView?.indexPath(for: sender as!UICollectionViewCell)
            if let nextVC = segue.destination as? FilmeTableViewController {
                nextVC.filme = listaFilmes[(selectRow?.row)!]
            }
        }
    }
    
    func getFilmesPopulares(pagina: Int){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "trakt-api-version": "2",
            "trakt-api-key": "70493fc5ec1a5e1b6cf58c41146f98714129bfb27fb7688057fa202b857d83b3"
        ]
        
        Alamofire.request("https:/api.trakt.tv/movies/popular?page=\(pagina)&limit=10", headers: headers).responseString { response in
            switch response.result {
            case .success:
                self.pullToRefresh = false
                if(response.response?.statusCode == 200){
                    self.jsonSaveListFilmes(response.data!)
                }
            case .failure(let error):
                self.pullToRefresh = false
                print(error)
            }
        }
    }
    
    func jsonSaveListFilmes(_ json:Data){

        if let filmes = try? JSONSerialization.jsonObject(with: json) as! [AnyObject]{
            if(!filmes.isEmpty){
                for index in 0 ..< filmes.count {
                    let filme = Filme()
                    filme.title = filmes[index]["title"] as? String
                    filme.year = filmes[index]["year"] as? Int
                    
                    let ids = filmes[index]["ids"] as! NSDictionary
                    let id = Id()
                    id.tmdb = ids.value(forKey: "tmdb") as? Int
                    filme.ids = id
                    getOutrasInformacoes(filme: filme)
                }
                
                self.collectionView.reloadData()
                pagina += 1
            }
        }
    }
    
    func getBuscarFilmes(termo: String){
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "trakt-api-version": "2",
            "trakt-api-key": "70493fc5ec1a5e1b6cf58c41146f98714129bfb27fb7688057fa202b857d83b3"
        ]
        
        Alamofire.request("https://api.trakt.tv/search/movie?query=\(termo)", headers: headers).responseString { response in
            switch response.result {
            case .success:
                if(response.response?.statusCode == 200){
                    self.jsonSaveBuscaListFilmes(response.data!)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func jsonSaveBuscaListFilmes(_ json:Data){
        
        if let filmes = try? JSONSerialization.jsonObject(with: json) as! [AnyObject]{
            if(!filmes.isEmpty){
                for index in 0 ..< filmes.count {
                    let movie = filmes[index]["movie"] as! NSDictionary
                    let filme = Filme()
                    
                    filme.title = movie.value(forKey: "title") as? String
                    filme.year = movie.value(forKey: "year") as? Int
                    
                    let ids = movie.value(forKey: "ids") as! NSDictionary
                    let id = Id()
                    id.tmdb = ids.value(forKey: "tmdb") as? Int
                    filme.ids = id
                    getOutrasInformacoes(filme: filme)

                }
                
                self.collectionView.reloadData()
            }
        }
    }
    
    func getOutrasInformacoes(filme: Filme){
        Alamofire.request("https://api.themoviedb.org/3/movie/\(filme.ids!.tmdb!)?api_key=9903ff3d1fe76928805991fc3955d701").responseString { response in
            switch response.result {
            case .success:
                if(response.response?.statusCode == 200){
                    if let infos = try? JSONSerialization.jsonObject(with: response.data!) as AnyObject{
                        filme.capa = infos["poster_path"] as? String
                        filme.runtime = infos["runtime"] as? Int
                        filme.tagline = infos["tagline"] as? String
                        filme.overview = infos["overview"] as? String
                        filme.backdrop_path = infos["backdrop_path"] as? String
                        
                        var listGenres = [Genre]()
                        let genres = infos["genres"] as! [AnyObject]
                        for index in 0 ..< genres.count{
                            let genre = Genre()
                            genre.name = genres[index]["name"] as? String
                            
                            listGenres.append(genre)
                        }
                        filme.genres = listGenres
                        
                        
                        
                        
                        self.listaFilmes.append(filme)
                        self.collectionView.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
