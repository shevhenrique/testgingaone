//
//  FilmeCollectionViewCell.swift
//  Dfilmes
//
//  Created by Henrique Mota on 06/09/17.
//  Copyright © 2017 hkmobi. All rights reserved.
//

import UIKit

class FilmeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imagemFilme: UIImageView!
    @IBOutlet weak var nomeFilme: UILabel!
    @IBOutlet weak var anoFilme: UILabel!
    
}
