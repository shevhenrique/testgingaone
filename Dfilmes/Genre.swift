//
//  Genre.swift
//  Dfilmes
//
//  Created by Henrique Mota on 07/09/17.
//  Copyright © 2017 hkmobi. All rights reserved.
//

import Foundation

class Genre{
    var id: Int?
    var name: String?
}
