//
//  FilmeTableViewController.swift
//  Dfilmes
//
//  Created by Henrique Mota on 07/09/17.
//  Copyright © 2017 hkmobi. All rights reserved.
//

import UIKit
import ImageSlideshow

class FilmeTableViewController: UITableViewController {

    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var anoLancamento: UILabel!
    @IBOutlet weak var genero: UILabel!
    @IBOutlet weak var tempo: UILabel!
    @IBOutlet weak var tagline: UILabel!
    @IBOutlet weak var sinopse: UILabel!
    
    var filme = Filme()
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBOutlet var slideshow: ImageSlideshow!
    var alamofireSource = [InputSource]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = filme.title
        titulo.text = filme.title
        anoLancamento.text = "Lançamento: \(filme.year!)"
        tempo.text = "\(filme.runtime!) min"
        tagline.text = filme.tagline
        sinopse.text = filme.overview
        
        var genres = ""
        for index in 0 ..< filme.genres!.count {
            if(index == 0){
                genres += filme.genres![index].name!
            }else if(index == filme.genres!.count - 1){
                genres += " - \(filme.genres![index].name!)"
            }else{
                genres += " - \(filme.genres![index].name!)"
            }
        }
        
        genero.text = genres
        
        let link_image = "https://image.tmdb.org/t/p/w300\(filme.backdrop_path!)"
        let link_image2 = "https://image.tmdb.org/t/p/w300\(filme.capa!)"
        
        alamofireSource.append(AlamofireSource(urlString: link_image)!)
        alamofireSource.append(AlamofireSource(urlString: link_image2)!)
        
        slideshow.backgroundColor = UIColor.black
        slideshow.zoomEnabled = false
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFit
        slideshow.setImageInputs(alamofireSource)
        
    }

    
    @IBAction func compartilhar(_ sender: Any) {
        DispatchQueue.main.async{
            let viewController = UIActivityViewController(activityItems: ["Já assistiu \(self.filme.title!)? Conheça esse filme e outros em:", URL.init(string: "https://gingaone.com/pt-br/")!], applicationActivities: nil)
            viewController.popoverPresentationController?.sourceView = self.view
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
