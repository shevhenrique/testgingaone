//
//  Filme.swift
//  Dfilmes
//
//  Created by Henrique Mota on 06/09/17.
//  Copyright © 2017 hkmobi. All rights reserved.
//

import Foundation

class Filme{
    var title: String?
    var year: Int?
    var ids: Id?
    var capa: String?
    var genres: [Genre]?
    var runtime: Int?
    var tagline: String?
    var overview: String?
    var backdrop_path: String?
    
}
