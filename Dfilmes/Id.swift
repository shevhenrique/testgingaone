//
//  Id.swift
//  Dfilmes
//
//  Created by Henrique Mota on 06/09/17.
//  Copyright © 2017 hkmobi. All rights reserved.
//

import Foundation

class Id{
    var trakt: Int?
    var slug: String?
    var imdb: String?
    var tmdb: Int?
}
